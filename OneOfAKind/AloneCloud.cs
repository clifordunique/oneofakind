﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using SdlDotNet;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Audio;

namespace OneOfAKind
{
    /// <summary>
    /// Cloud thing that "aloneizes" whatever it hits.
    /// </summary>
    public class AloneCloud : AnimatedSprite, Deletable
    {
        /// <summary>
        /// Duration of the cloud, in ticks.
        /// </summary>
        const int duration = 60;

        /// <summary>
        /// Whether this is owned by the reflection.
        /// </summary>
        public bool Reflection;

        bool active = true;

        int durationTimer = 0;

        // length of each animation frame.
        const int animationRate = 2;

        int animationTimer = 0;

        // Level that this cloud is in.
        Level level;

        public AloneCloud(Point location, Level level, bool reflection) {
            this.Reflection = reflection;
            this.X = location.X - 10;
            this.Y = location.Y - 10;
            this.level = level;

            SurfaceCollection tempSurf;

            tempSurf = new SurfaceCollection();
            tempSurf.Add("img/cloud.png", new Size(20, 20), 0);
            foreach (Surface surface in tempSurf) { surface.Convert(Video.Screen); }
            AnimationCollection cloudAnim = new AnimationCollection();
            cloudAnim.Add(tempSurf);

            Animations.Add("cloud", cloudAnim);
            this.AlphaBlending = true;
            this.TransparentColor = Color.FromArgb(255, 0, 255);
            this.Transparent = true;

            CurrentAnimation = "cloud";
        }

        bool IntersectsWithReflection(Sprite sprite) {
            foreach (Sprite other in OneOfAKind.CurrentLevel().Sprites) {
                if (other is Reflection) {
                    if (other.IntersectsWith(sprite)) {
                        return true;
                    }
                }
            }

            return false;
        }

        public static Sound flashSound = new Sound("sfx/disappear.ogg");

        public string IntersectingWith() {
            foreach (Sprite sprite in OneOfAKind.CurrentLevel().Sprites) {
                if (sprite is Disappearable && this.IntersectsWith(sprite)) {
                    return ((Disappearable)sprite).DisappearKey();
                }
            }

            return "";
        }

        public override void Update(SdlDotNet.Core.TickEventArgs args) {
            base.Update(args);

            bool flash = false;

            if (OneOfAKind.CurrentLevelIndex == Constants.Levels - 1) {
                //endgame stuff
                if (!OneOfAKind.EndGame) {
                    foreach (Sprite sprite in OneOfAKind.CurrentLevel().Sprites) {
                        if (sprite is Reflection && !this.Reflection && this.IntersectsWith(sprite)) {
                            OneOfAKind.Ending(true);
                        }
                    }

                    if (this.Reflection) {
                        if (this.IntersectsWith(OneOfAKind.Player)) {
                            OneOfAKind.Ending(false);
                        }
                    }
                }
            }

            if (active && level == OneOfAKind.CurrentLevel()) {

                foreach (Sprite sprite in OneOfAKind.CurrentLevel().Sprites) {
                    if (sprite is Disappearable && this.IntersectsWith(sprite)) {
                        string key = ((Disappearable)sprite).DisappearKey();
                        foreach (Sprite other in OneOfAKind.CurrentLevel().Sprites) {
                            if (sprite == other) {
                                continue;
                            }

                            if (other is Disappearable) {
                                if (((Disappearable)other).DisappearKey() == key &&
                                    ((Disappearable)other).IsVisible() && !sprite.IntersectsWith(OneOfAKind.Player) &&
                                    !IntersectsWithReflection(sprite)) {

                                    ((Disappearable)other).SetVisible(false);
                                    flash = true;
                                    ((Disappearable)sprite).SetVisible(true);

                                    foreach (Sprite mightBeCloud in OneOfAKind.CurrentLevel().Sprites) {
                                        if (mightBeCloud is AloneCloud) {
                                            if (((AloneCloud)mightBeCloud).IntersectingWith() == key &&
                                                key != "") {
                                                ((AloneCloud)mightBeCloud).active = false;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }

            if (flash) {
                // add screenflash
                OneOfAKind.ScreenFlash();

                if (SdlDotNet.Audio.Mixer.FindAvailableChannel() > -1) {
                    flashSound.Volume = 64;
                    flashSound.Play();
                }
                active = false;
            }

            durationTimer++;
            animationTimer++;
            if (durationTimer < duration / 2) {
                this.Alpha = (byte)255;
            } else {
                this.Alpha = (byte)(255 * (1.0f - ((float)((durationTimer - duration) * 2) / duration)));
            }

            if (animationTimer >= animationRate) {
                this.Frame++;
                animationTimer = 0;
            }
        }

        public void MarkForDeletion() {
        }

        public bool ShouldDelete() {
            return durationTimer >= duration;
        }
    }
}

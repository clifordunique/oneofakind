﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using SdlDotNet;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Audio;

namespace OneOfAKind
{
    /// <summary>
    /// Endgame object.
    /// </summary>
    public class Mirror : Sprite
    {
        public Mirror(Point location)
            : base("img/mirror.png") {
            this.Surface.Convert(Video.Screen);
            this.Position = location;
        }

        public override void Update(SdlDotNet.Core.TickEventArgs args) {
            base.Update(args);

            foreach (Sprite sprite in OneOfAKind.CurrentLevel().Sprites) {
                if (sprite is AloneCloud && sprite.IntersectsWith(this)) {
                    OneOfAKind.Ending(true);
                    return;
                }
            }
        }
    }
}

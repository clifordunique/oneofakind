﻿using System;
using System.Drawing;
using System.Collections.Generic;

using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Input;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Audio;

namespace OneOfAKind
{
    public class OneOfAKind
    {
        // Game objects go here.
        public static int CurrentLevelIndex;
        public static List<Level> Levels = new List<Level>();
        public static Player Player;
        public static float LevelSpawnPointX = Constants.PlayerStartCoords.X;
        public static float LevelSpawnPointY = Constants.PlayerStartCoords.Y;

        public static Message IntroMessage;

        public static bool EndGame = false;

        public static int EndingTimer = 0;

        static public Level CurrentLevel() {
            return Levels[CurrentLevelIndex];
        }

        // Init all the game objects.
        void GameInit() {
            for (int i = 0; i < Constants.Levels; i++) {
                Levels.Add(Level.LoadLevel(i));
            }
            CurrentLevelIndex = -1;
            Player = new Player(Constants.PlayerStartCoords);

            //DEBUG:
            CurrentLevelIndex = 16;
            Player = new Player(new Point(Constants.TileSize *8, Constants.TileSize * 20));
            CurrentLevel().Sprites.Add(new Mirror(new Point(Player.X + 50, Player.Y)));
        }

        // setup video mode so we can init surfaces statically.
        public OneOfAKind() {
            Video.SetVideoMode(Constants.WindowWidth, Constants.WindowHeight, false, false, false);
            Video.WindowCaption = "One Of A Kind v" + Constants.Version + " by DDRKirby(ISQ)";

            Mixer.Open(44100, AudioFormat.Default, SoundChannel.Stereo, 4096);
            Mixer.ChannelsAllocated = 32;
        }

        public static void Main() {
            OneOfAKind game = new OneOfAKind();
            game.GameInit();
            game.Run();
        }

        static Music music = new Music("mus/oneofakind.ogg");
        static Music endMusic = new Music("mus/ending.ogg");

        public void Run() {
            // Init our member vars.
            flashSurface = new Surface("img/flash.png");
            flashSurface.AlphaBlending = true;
            flashSurface.Convert(Video.Screen);
            aquaFlashSurface = new Surface("img/aquaflash.png");
            aquaFlashSurface.AlphaBlending = true;
            aquaFlashSurface.Convert(Video.Screen);
            fadeSurface = new Surface("img/fade.png");
            fadeSurface.AlphaBlending = true;
            fadeSurface.Convert(Video.Screen);

            IntroMessage = new Message("I am not very special.", Level.textFont, Color.White, new Point(240, 230), 0, 400, -1);
            IntroMessage.Alpha = (byte)255;

            music.Play(true);
            
            Events.Quit += new EventHandler<QuitEventArgs>(this.Quit);
            Events.TargetFps = 6000;
            Events.Tick += new EventHandler<TickEventArgs>(this.Tick);

            Events.KeyboardDown += new EventHandler<KeyboardEventArgs>(this.KeyboardDown);
            Events.MouseButtonDown += new EventHandler<MouseButtonEventArgs>(this.MouseDown);

            Events.Run();
        }

        void MouseDown(object sender, MouseButtonEventArgs eventArgs) {
            if (CurrentLevelIndex == -1) {
                // Transition into game upon keypress.
                CurrentLevelIndex = 0;

                screenFadeTimer = screenFadeDuration;

                return;
            }
        }

        void Quit(object sender, QuitEventArgs e) {
            Events.QuitApplication();
        }

        void KeyboardDown(object sender, KeyboardEventArgs eventArgs) {
            if (CurrentLevelIndex == -1) {
                // Transition into game upon keypress.
                CurrentLevelIndex = 0;

                screenFadeTimer = screenFadeDuration;

                return;
            }

            if (EndGame) {
                return;
            }

            Player.Update(eventArgs);
            for (int i = 0; i < CurrentLevel().Sprites.Count; i++ ) {
                Sprite sprite = CurrentLevel().Sprites[i];
                if (sprite is Reflection) {
                    sprite.Update(eventArgs);
                }
            }
        }

        // Tick time accumulated.
        double accumulator = 0;

        // Ticks per frame.
        const double ticksPerFrame = 1000.0 / Constants.FPS;

        void Tick(object sender, TickEventArgs eventArgs) {
            // TODO: fancy lerp stuff

            // Add elapsed ticks to accumulator.
            accumulator += eventArgs.TicksElapsed;

            // Do nothing if not enough time passed.
            if (accumulator < ticksPerFrame) {
                return;
            }

            // Take as many logic steps as necessary.
            while (accumulator >= ticksPerFrame) {
                Update(eventArgs);
                accumulator -= ticksPerFrame;
            }

            Draw(Video.Screen);

            Video.Screen.Update();
        }


        static SdlDotNet.Graphics.Font endFont = new SdlDotNet.Graphics.Font("fnt/font.ttf", 20);

        /// <summary>
        /// Does update for one logical timestep.
        /// </summary>
        void Update(TickEventArgs eventArgs) {
            if (CurrentLevelIndex == -1) {
                IntroMessage.Update(eventArgs);
                return;
            }

            if (EndGame) {
                EndingTimer++;
                if (EndingTimer == 420) {
                    endMusic.Play();
                }

                if (EndingTimer == 420) {
                    Message m = new Message("One of a Kind", endFont, Color.White, new Point(250, 220), -10.0f, 300, CurrentLevelIndex);
                    EndMessages.Add(m);
                }
                if (EndingTimer == 620) {
                    EndMessages[0].FadeOut();
                }

                if (EndingTimer == 720) {
                    Message m = new Message("Programmed by DDRKirby(ISQ)", endFont, Color.White, new Point(250, 220), -10.0f, 300, CurrentLevelIndex);
                    EndMessages.Add(m);
                }
                if (EndingTimer == 920) {
                    EndMessages[1].FadeOut();
                }

                if (EndingTimer == 1020) {
                    Message m = new Message("Music by DDRKirby(ISQ)", endFont, Color.White, new Point(210, 220), -10.0f, 300, CurrentLevelIndex);
                    EndMessages.Add(m);
                }
                if (EndingTimer == 1220) {
                    EndMessages[2].FadeOut();
                }

                if (EndingTimer == 1320) {
                    Message m = new Message("Art by DDRKirby(ISQ)", endFont, Color.White, new Point(215, 220), -10.0f, 300, CurrentLevelIndex);
                    EndMessages.Add(m);
                }
                if (EndingTimer == 1520) {
                    EndMessages[3].FadeOut();
                }

                if (EndingTimer == 1620) {
                    Message m = new Message("Developed in 48 hours for Ludum Dare 22", endFont, Color.White, new Point(220, 220), -10.0f, 230, CurrentLevelIndex);
                    EndMessages.Add(m);
                }
                if (EndingTimer == 2020) {
                    EndMessages[4].FadeOut();
                }


                foreach (Message m in EndMessages) {
                    m.Update(eventArgs);
                }

                if (EndingTimer == 2120) {
                    MusicPlayer.Fadeout(6000);
                }
                if (EndingTimer == 2620) {
                    Events.QuitApplication();
                }

                return;
            }

            // Update player.
            Player.Update(eventArgs);

            // Update level objects.
            for (int i = CurrentLevelIndex - 1; i <= CurrentLevelIndex + 1; i++) {
                if (i >= 0 && i < Levels.Count) {
                    Levels[i].Update(eventArgs);
                }
            }

            // Level transitions.
            if (Player.Center.X > Constants.PlayfieldWidth) {
                CurrentLevelIndex++;

                if (CurrentLevelIndex == Levels.Count - 1) {
                    // fade out music
                    if (!MusicPlayer.IsFading && MusicPlayer.IsPlaying) {
                        MusicPlayer.Fadeout(1000);
                    }
                }

                Player.realX = Player.realX - Constants.PlayfieldWidth;

                //prevent glitching
                Player.realX++;
                Player.X = (int)Player.realX;

                LevelSpawnPointX = Player.realX;
                LevelSpawnPointY = Player.realY;
            }

            if (Player.Center.X < 0) {
                CurrentLevelIndex--;
                Player.realX = Player.realX + Constants.PlayfieldWidth;

                //prevent glitching
                Player.realX--;
                Player.X = (int)Player.realX;

                LevelSpawnPointX = Player.realX;
                LevelSpawnPointY = Player.realY;
            }

            // sanity check
            while (CurrentLevelIndex < 0) {
                CurrentLevelIndex++;
            }
            while (CurrentLevelIndex >= Levels.Count) {
                CurrentLevelIndex--;
            }

            // screen flash
            if (screenFlashTimer > 0) {
                screenFlashTimer--;
            }
            if (screenFadeTimer > 0) {
                screenFadeTimer--;
            }

        }

        const int screenFlashDuration = Constants.ScreenFlashDuration;
        static int screenFlashTimer = 0;
        const int screenFadeDuration = Constants.ScreenFadeDuration;
        static int screenFadeTimer = 0;
        
        static public void ScreenFlash() {
            screenFlashTimer = screenFlashDuration;
        }

        // Backdrop
        static Surface flashSurface;
        static Surface aquaFlashSurface;
        static Surface fadeSurface;

        static List<Message> EndMessages = new List<Message>();

        void Draw(Surface destination) {
            if (CurrentLevelIndex == -1) {
                destination.Fill(Color.Black);
                destination.Blit(IntroMessage);
                return;
            }
            if (EndGame) {
                destination.Fill(Color.Black);

                if (EndingTimer < 120) {
                    if (regularEnd) {
                        destination.Fill(Color.White);
                    } else {
                        destination.Fill(Color.FromArgb(0, 255, 255));
                    }
                } else if (EndingTimer < 360) {
                    if (regularEnd) {
                        destination.Fill(Color.White);
                    } else {
                        destination.Fill(Color.FromArgb(0, 255, 255));
                    }
                    fadeSurface.Alpha = (byte)(255 * ((EndingTimer - 120) / 240.0));
                    destination.Blit(fadeSurface);
                }

                foreach (Message message in EndMessages) {
                    destination.Blit(message);
                }


                return;
            }

            destination.Blit(CurrentLevel().Backdrop);
            CurrentLevel().Draw(destination);
            destination.Blit(Player);

            // screenflash
            if (screenFlashTimer > 0) {
                if (EndGame && !regularEnd) {
                    aquaFlashSurface.Alpha = (byte)(Constants.ScreenFlashAlpha * (float)(screenFlashTimer) / screenFlashDuration);
                    destination.Blit(aquaFlashSurface);
                } else {
                    flashSurface.Alpha = (byte)(Constants.ScreenFlashAlpha * (float)(screenFlashTimer) / screenFlashDuration);
                    destination.Blit(flashSurface);
                }
            }
            if (screenFadeTimer > 0) {
                fadeSurface.Alpha = (byte)(255 * (float)(screenFadeTimer) / screenFadeDuration);
                destination.Blit(fadeSurface);
            }
        }

        static bool regularEnd;

        static public void Ending(bool regular) {
            regularEnd = regular;
            EndGame = true;
            if (SdlDotNet.Audio.Mixer.FindAvailableChannel() > -1) {
                AloneCloud.flashSound.Volume = 64;
                AloneCloud.flashSound.Play();
            }
        }
    }
}
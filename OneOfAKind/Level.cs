﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using SdlDotNet;
using SdlDotNet.Core;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;

namespace OneOfAKind
{
    /// <summary>
    /// For representing one screen of the game.
    /// </summary>
    public class Level
    {
        public Surface Backdrop;
        public SpriteCollection Sprites = new SpriteCollection();

        public Point ReflectionSpawn;

        public static SdlDotNet.Graphics.Font textFont = new SdlDotNet.Graphics.Font("fnt/font.ttf", 10);

        public static Level LoadLevel(int number) {
            Level result = new Level("lvl/level" + (number + 1) + ".png", "lvl/level" + (number + 1) + "_backdrop.png", number);


            String text;
            Point point;
            float start;
            int width;
            switch (number+1) {
                case 1:
                    text = "I can move Left and Right using the arrow keys.";
                    point = new Point(100, 80);
                    start = 50;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    text = "This is not very special.";
                    point = new Point(400, 90);
                    start = 300;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    break;
                case 2:
                    text = "I can jump by pressing Up.";
                    point = new Point(100, 90);
                    start = 50;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    text = "This, too, is not very special.";
                    point = new Point(380, 90);
                    start = 300;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    break;
                case 3:
                    text = "These jumps look tricky.";
                    point = new Point(90, 90);
                    start = 30;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    text = "Thankfully, I can press Escape to try again.";
                    point = new Point(340, 90);
                    start = 160;
                    width = 400;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    break;
                case 4:
                    text = "Each room I find is different.";
                    point = new Point(90, 90);
                    start = 50;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    text = "I am good at noticing what makes things unique.";
                    point = new Point(385, 80);
                    start = 300;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    break;
                case 5:
                    text = "These two blocks may seem the same to you.";
                    point = new Point(80, 80);
                    start = 50;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    text = "But when I press Space, I can see that each one is one of a kind.";
                    point = new Point(380, 80);
                    start = 300;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    break;
                case 6:
                    text = "I can find what is special about anything.";
                    point = new Point(80, 80);
                    start = 50;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    text = "No matter what shape, size, or color.";
                    point = new Point(380, 80);
                    start = 300;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    break;
                case 7:
                    text = "This puzzle looks familiar.";
                    point = new Point(90, 90);
                    start = 50;
                    width = 300;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    text = "But I know that it is special in its own way.";
                    point = new Point(370, 80);
                    start = 300;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                    break;
                case 8:
                    text = "Sometimes it may surprise you to realize something is special.";
                    point = new Point(200, 40);
                    start = 50;
                    width = 200;
                    result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 9:
                   text = "Other times, you may have to try harder to notice it.";
                   point = new Point(110, 30);
                   start = 50;
                   width = 200;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 10:
                   text = "Sometimes it can help to look at something from both sides.";
                   point = new Point(90, 50);
                   start = 50;
                   width = 300;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 11:
                   text = "But...once you see something in a certain light, you may not be able to approach it in the same way again.";
                   point = new Point(165, 40);
                   start = 50;
                   width = 300;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 12:
                   text = "Then, perhaps it is better to simply not look at some things at all.";
                   point = new Point(170, 40);
                   start = 50;
                   width = 300;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 13:
                   text = "There is a reflection of me now.  It is special too.";
                   point = new Point(80, 200);
                   start = 50;
                   width = 200;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   text = "It can do things that I cannot.";
                   point = new Point(360, 210);
                   start = 200;
                   width = 300;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 14:
                   text = "Sometimes I think that my reflection is capable of handling more than I am.";
                   point = new Point(140, 260);
                   start = 50;
                   width = 300;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 15:
                   text = "At times, I even feel like it is being rewarded.";
                   point = new Point(140, 260);
                   start = 50;
                   width = 200;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 16:
                   text = "There is definitely nothing special about me.";
                   point = new Point(170, 260);
                   start = 50;
                   width = 200;
                   result.Sprites.Add(new Message(text, textFont, Color.White, point, start, width, number));
                   break;
                case 17:
                   break;
                default:
                    System.Diagnostics.Debug.Assert(false);
                    break;
            }

            return result;
        }

        /// <summary>
        /// Loads a level from the given bitmap.
        /// </summary>
        /// <param name="bitmap">Bitmap to load from.</param>
        public Level(string bitmap, string backdrop, int levelIndex) {
            Surface image = new Surface(bitmap);
            Backdrop = new Surface(backdrop);
            Backdrop.Convert(Video.Screen);
            Backdrop.Transparent = true;
            Backdrop.TransparentColor = Color.FromArgb(255, 0, 255);

            for (int x = 0; x < image.Width; ++x) {
                for (int y = 0; y < image.Height; ++y) {
                    int realX = x * Constants.TileSize;
                    int realY = y * Constants.TileSize;
                    Color color = image.GetPixel(new Point(x, y));
                    if (color == Color.FromArgb(0, 0, 0)) {
                        // Empty tile.
                    } else if (color == Color.FromArgb(255, 255, 255)) {
                        // TileBlock.
                        Sprites.Add(new TileBlock(x, y));
                    } else if (color == Color.FromArgb(0, 255, 255)) {
                        Sprites.Add(new ColorWall(new Point(realX + Constants.TileSize / 2, realY), "cyan_wall", "img/cyan_wall.png", "img/cyan_wall_d.png"));
                    } else if (color == Color.FromArgb(0, 255, 0)) {
                        Sprites.Add(new ColorWall(new Point(realX + Constants.TileSize / 2, realY), "green_wall", "img/green_wall.png", "img/green_wall_d.png"));
                    } else if (color == Color.FromArgb(255, 0, 0)) {
                        Sprites.Add(new ColorWall(new Point(realX + Constants.TileSize / 2, realY), "red_wall", "img/red_wall.png", "img/red_wall_d.png"));
                    } else if (color == Color.FromArgb(0, 0, 255)) {
                        Sprites.Add(new ColorWall(new Point(realX + Constants.TileSize / 2, realY), "blue_wall", "img/blue_wall.png", "img/blue_wall_d.png"));
                    } else if (color == Color.FromArgb(255, 255, 0)) {
                        Sprites.Add(new ColorWall(new Point(realX + Constants.TileSize / 2, realY), "yellow_wall", "img/yellow_wall.png", "img/yellow_wall_d.png"));
                    } else if (color == Color.FromArgb(255, 0, 255)) {
                        Sprites.Add(new ColorWall(new Point(realX + Constants.TileSize / 2, realY), "purple_wall", "img/purple_wall.png", "img/purple_wall_d.png"));
                    } else if (color == Color.FromArgb(0, 128, 128)) {
                        Sprites.Add(new ColorWall(new Point(realX, realY), "cyan_block", "img/cyan_block.png", "img/cyan_block_d.png"));
                    } else if (color == Color.FromArgb(0, 128, 0)) {
                        Sprites.Add(new ColorWall(new Point(realX, realY), "green_block", "img/green_block.png", "img/green_block_d.png"));
                    } else if (color == Color.FromArgb(128, 0, 0)) {
                        Sprites.Add(new ColorWall(new Point(realX, realY), "red_block", "img/red_block.png", "img/red_block_d.png"));
                    } else if (color == Color.FromArgb(0, 0, 128)) {
                        Sprites.Add(new ColorWall(new Point(realX, realY), "blue_block", "img/blue_block.png", "img/blue_block_d.png"));
                    } else if (color == Color.FromArgb(128, 128, 0)) {
                        Sprites.Add(new ColorWall(new Point(realX, realY), "yellow_block", "img/yellow_block.png", "img/yellow_block_d.png"));
                    } else if (color == Color.FromArgb(128, 0, 128)) {
                        Sprites.Add(new ColorWall(new Point(realX, realY), "purple_block", "img/purple_block.png", "img/purple_block_d.png"));
                    } else if (color == Color.FromArgb(128, 128, 128)) {
                        // reflection start
                        Sprites.Add(new Reflection(new Point(realX, realY), levelIndex, false));
                        ReflectionSpawn = new Point(realX, realY);
                    } else if (color == Color.FromArgb(192, 192, 192)) {
                        // endgame reflection start
                        Sprites.Add(new Reflection(new Point(realX, realY), levelIndex, true));
                        ReflectionSpawn = new Point(realX, realY);
                    } else {
                        // shouldn't happen.
                        System.Diagnostics.Debug.Assert(false);
                    }
                    
                }
            }
            // TODO: do stuff here
        }

        public void Draw(Surface dest) {
            dest.Blit(Sprites);
        }

        public void Update(TickEventArgs eventArgs) {
            for (int i = 0; i < Sprites.Count; i++) {
                // process player aloneclouds at the end

                if (Sprites[i] is AloneCloud && !((AloneCloud)Sprites[i]).Reflection) {
                    continue;
                }
                Sprites[i].Update(eventArgs);
            }

            for (int i = 0; i < Sprites.Count; i++) {
                // process player aloneclouds

                if (Sprites[i] is AloneCloud && !((AloneCloud)Sprites[i]).Reflection) {
                    Sprites[i].Update(eventArgs);
                }
            }

            // Delete things that need to be deleted
            for (int i = Sprites.Count - 1; i >= 0; i--) {
                if (Sprites[i] is Deletable) {
                    if (((Deletable)Sprites[i]).ShouldDelete()) {
                        Sprites.RemoveAt(i);
                    }
                }
            }
        }
    }
}